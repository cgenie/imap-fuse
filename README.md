# `imap-fuse` -- a FUSE filesystem for IMAP

```
opam install imap lwt lwt_ppx ocamlfuse ssl 
```

Useful links:

- [CS135 FUSE Documentation](https://www.cs.hmc.edu/~geoff/classes/hmc.cs135.201001/homework/fuse/fuse_doc.html)
- [`memfs-fuse`](https://github.com/tniessen/memfs-fuse)
- [Develop your own filesystem with FUSE](https://developer.ibm.com/articles/l-fuse/)
- [Writing a FUSE filesystem: Tutorial](https://www.cs.nmsu.edu/~pfeiffer/fuse-tutorial/)
- [`imap-fuse` (in Python)](https://github.com/keis/imap-fuse)


## Raw filesystem spec

```
.
by_date/
by_from/
by_subject/
by_to/
by_uid/
```

## Random notes

- `ocamlfuse` is quite unmaintained. In particular it doesn't support FUSE's
  `private_data` structure which can be quite useful.
- I don't know how to integrate `LWT` nicely -- `ocaml-imap` is LWT-ready but
  `ocamlfuse` isn't.
- Caching seems necessary: with large mailboxes, fetching the UIDs everytime
  seems an overkill. At the same time IMAP process should be running and
  updating the cache as new mail arrives.
