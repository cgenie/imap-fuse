(* Globals *)
let start_time = Unix.gettimeofday ()
let verbose = ref false
let log_channel = ref stdout

(* Logging *)
let log_message format =
  if !verbose then
    Printf.fprintf !log_channel format
  else
    Printf.ifprintf !log_channel format

let log_with_header format =
  let elapsed = Unix.gettimeofday () -. start_time in
  log_message "[%f] " elapsed;
  log_message format

let log_exception e =
  let message = Printexc.to_string e in
  let backtrace = Printexc.get_backtrace () in
  log_with_header "Exception:%s\n" message;
  log_message "Backtrace:%s\n%!" backtrace


(* Unix *)
let flags_to_string flags =
  let flag_descriptions =
    List.map
      (function
         | Unix.O_RDONLY -> "O_RDONLY"
         | Unix.O_WRONLY -> "O_WRONLY"
         | Unix.O_RDWR -> "O_RDWR"
         | Unix.O_NONBLOCK -> "O_NONBLOCK"
         | Unix.O_APPEND -> "O_APPEND"
         | Unix.O_CREAT -> "O_CREAT"
         | Unix.O_TRUNC -> "O_TRUNC"
         | Unix.O_EXCL -> "O_EXCL"
         | Unix.O_NOCTTY -> "O_NOCTTY"
         | Unix.O_DSYNC -> "O_DSYNC"
         | Unix.O_SYNC -> "O_SYNC"
         | Unix.O_RSYNC -> "O_RSYNC"
         (* Only in OCaml 4
         | Unix.O_SHARE_DELETE -> "O_SHARE_DELETE" *)
         | _ -> "_")
      flags
  in
  String.concat "," flag_descriptions
