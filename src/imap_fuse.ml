(* open Lwt.Infix *)

type imap_config = {
  server : string;
  port : int;
  login : string;
  password : string;
  mbox : string;
}

let io_buffer_size = 65536

let mk_imap_config srv pt lgn pass mbox = {
  server = srv;
  port = pt;
  login = lgn;
  password = pass;
  mbox = mbox;
}

let statfs path =
  Utils.log_with_header "statfs %s\n" path
  (*
  try
    Imap.statfs ()
  with e ->
    Utils.log_exception e;
    han
  *)

let current_dir = ref "."


let list_uids imap =
  let%lwt uids, _ = Imap.uid_search imap Imap.Search.all in
  Lwt.return uids

let readdir imap path hnd =
  Utils.log_with_header "readdir: %s [%d]\n" path hnd;
  let uids = Lwt_main.run (list_uids imap) in
  if path = "/" then
    ["."; ".."; "by_uid"]
  else if path = "/by_uid" then
    List.append ["."; ".."] (List.map Int32.to_string uids)
  else raise Unix.(Unix_error (ENOENT, "stat", path))

let opendir imap path flags =
  Utils.log_with_header "opendir: %s [%s]\n" path (Utils.flags_to_string flags);
  current_dir := path;
  None

let default_stats = Unix.LargeFile.stat "."

let getattr imap path =
  Utils.log_with_header "getattr: %s; current_dir: %s\n" path !current_dir;
  (* default_stats *)

  let stats = if path = "." or path = ".." or path = "/" or path == "by_uid" then
    default_stats
  else begin
    let uids = Lwt_main.run (list_uids imap) in
    let uids_str = List.map Int32.to_string uids in

    match (List.find_opt (fun uid -> ("/by_uid" ^ uid) = path) uids_str) with
      | Some uid -> {
          default_stats with
          st_ino = int_of_string uid;
          st_nlink = 1;
          st_kind = Unix.S_REG;
          st_perm = 0o644;
          st_size = 0L;
        }
      | None -> raise Unix.(Unix_error (ENOENT, "getattr", path))
  end in

  stats

  (* Unix.LargeFile.{ *)
  (*   st_dev = 0; *)
  (*   st_ino = 0; *)
  (*   st_kind = kind; *)
  (*   st_perm = 0x644; *)
  (*   st_nlink = 1;  (\* 2 -- if is folder? *\) *)
  (*   st_uid = 1000; *)
  (*   st_gid = 1000; *)
  (*   st_rdev = 0; *)
  (*   st_size = 0L; *)
  (*   st_atime = 1388567583.000000; *)
  (*   st_mtime = 1388567583.000000; *)
  (*   st_ctime = 1388567583.000000; *)
  (* } *)

let start_imap imap_cfg =
  let%lwt imap = Imap.connect ~host:imap_cfg.server ~port:imap_cfg.port ~username:imap_cfg.login ~password:imap_cfg.password in
  let%lwt _ = Imap.examine imap imap_cfg.mbox in
  Lwt.return imap


(* TODO Rewrite: fetch messages concurrently via uid *)
let list_imap imap =
  let%lwt uids, _ = Imap.uid_search imap Imap.Search.all in
  (* Lwt_list.iter_s (fun (uid : Imap.uid) -> Lwt_io.printf "UID: %ld\n" uid) uids *)
  Lwt_list.iter_s (fun (uid: Imap.uid) ->
    (* TODO: messages could be nested folders then the BODYSTRUCTURE is something more complex (attachments etc) *)
    let to_fetch = [Imap.Fetch.Request.flags;
                    Imap.Fetch.Request.internaldate;
                    Imap.Fetch.Request.rfc822_size;
                    Imap.Fetch.Request.envelope;
                    Imap.Fetch.Request.bodystructure] in
    let msg = Imap.uid_fetch imap [uid] to_fetch in
    (* Lwt_stream.to_list msg >>= (fun resp -> *)
    msg (fun resp ->
      match resp with
      | {Imap.Fetch.Response.envelope = Some e;
         Imap.Fetch.Response.bodystructure = Some bs; _} -> begin
          Utils.log_with_header "UID: %ld\nSubject: %s\n" uid e.env_subject;
          match bs with
           (* Text ("PLAIN", _, _) *)
           | Text (_, bf, _) -> Utils.log_with_header "Text Body: %s\n" bf.fld_enc
           | _           -> ()
        end
      | _ ->
          Utils.log_with_header "Error fetching message %ld\n" uid
    )
  ) uids

let start_filesystem mountpoint imap_cfg fuse_args =
  let fuse_argv =
    Sys.argv.(0) :: (fuse_args @ [mountpoint])
    |> Array.of_list
  in

  (* TODO: spawn imap connection, then main loop with FUSE initialization, then call IMAP functions when needed in FUSE *)

  let imap = Lwt_main.run (start_imap imap_cfg) in

  Fuse.main fuse_argv {
    Fuse.default_operations with
      init = (fun () -> Utils.log_with_header "Filesystem started\n");
      readdir = readdir imap;
      opendir = opendir imap;
      getattr = getattr imap;
  }

  (* let%lwt _ = list_imap imap in *)
  (* Imap.disconnect imap *)

