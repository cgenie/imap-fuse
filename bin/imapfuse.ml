let () = Ssl.init()

let _ =
  let fuse_args = ref [] in
  let debug = ref false in
  let mountpoint = ref "./mnt" in
  let program = Filename.basename Sys.executable_name in
  let usage = Printf.sprintf "Usage: %s [options] [mountpoint]" program in
  (* See fuse options: https://www.cs.hmc.edu/~geoff/classes/hmc.cs135.201109/homework/fuse/fuse_doc.html *)
  let arg_specs =
    Arg.align (
      ["-verbose", Arg.Set Utils.verbose, " enable verbose logging on stdout. Default is false.";
       "-debug", Arg.Unit (fun () -> debug := true; Utils.verbose := true; fuse_args := "-d" :: !fuse_args; Printexc.record_backtrace true), " enable debug mode";]
    )
  in
  let server = ref "imap.gmail.com" in
    let port = ref 993 in
  let login = ref "xxx" in
  let password = ref "xxx" in
  let mbox = ref "INBOX" in
  let imap_cfg = Imap_fuse.mk_imap_config !server !port !login !password !mbox in
  let () = Arg.parse arg_specs (fun s -> mountpoint := s) usage in

  Utils.log_with_header "%s\n" usage;

  Imap_fuse.start_filesystem !mountpoint imap_cfg !fuse_args
